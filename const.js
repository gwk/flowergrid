
'use strict'; // tell the browser to catch as many errors as possible.

var pi = Math.PI;
var pi2 = Math.PI * 2;
var eps = 1e-300;

var tile_wh = 16 // tile width and height.
var tiles_s = {x:-1, y:-1}; // tiles size vec (number of tiles in each dimension).

var render_mode_debug = true;

// opaque identifier for render_callback.
// we can use this to remove the callback and end the simulation at any time.
var interval_callback_id = -1;

