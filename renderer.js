

function screenScaleFactor() {
  // detect if the screen is a retina display.
  if ('devicePixelRatio' in window) {
    if (window.devicePixelRatio > 1) {
      return window.devicePixelRatio;
    }
  }
  return 1;
}


function load_renderer(ctx) {
  log('load_canvas...');
  // set up the canvas context and initialize size variables.
  var canvas = document.getElementById('canvas');
  var canvas_s = Vec(canvas.width, canvas.height);
  tiles_s.x = Math.floor(canvas_s.x / tile_wh);
  tiles_s.y = Math.floor(canvas_s.y / tile_wh);

  var scaleFactor = screenScaleFactor();
  // to render at full resolution on a high-res display,
  // the canvas style must be set to the dimension in 'points.'
  canvas.style.width = canvas_s.x + 'px';
  canvas.style.height = canvas_s.y + 'px';
  // then the canvas size is scaled to the dimension in pixels.
  canvas.width *= scaleFactor;
  canvas.height *= scaleFactor;

  var ctx = canvas.getContext('2d');
  ctx.scale(scaleFactor, scaleFactor); // scale the rendering context for high-res displays.

  function draw_line(v0, v1) {
    // convenience function to draw a single line.
    ctx.beginPath();
    ctx.moveTo(v0.x, v0.y);
    ctx.lineTo(v1.x, v1.y);
    ctx.closePath();
    ctx.stroke();
  }

  function stroke_circle(p, radius) {
    ctx.beginPath();
    ctx.arc(p.x, p.y, radius, 0, pi2, true);
    ctx.closePath();
    ctx.stroke();
  }

  function fill_circle(p, radius) {
    ctx.beginPath();
    ctx.arc(p.x, p.y, radius, 0, pi2, true);
    ctx.closePath();
    ctx.fill();
  }

  function draw_tile(tile) {
    // remember the current transformation matrix (pushes a new matrix onto the matrix stack).
    ctx.save();
    ctx.translate(tile_wh * tile.p.x, tile_wh * tile.p.y);
    ctx.fillStyle = rand_col();
    ctx.fillRect(0, 0, tile_wh, tile_wh);
    ctx.fillStyle = rand_col();
    var l = tile_wh / 2;
    fill_circle(Vec(l, l), l * .667);
    ctx.restore();
  }


  function draw_info(sim_time, sim_interval, real_interval) {
    // setup for info overlay drawing.
    ctx.font = '12pt Menlo';
    ctx.textAlign = 'left';
    ctx.textBaseline = 'top';
    ctx.fillStyle = '#FFFFFF';
    ctx.fillText(sprintf('T:%.2f SI:%.2f RI:%.2f', sim_time, sim_interval, real_interval), 5, 5);
  }

  function render_scene(sim_time, sim_interval, real_interval, scn) {
    // render the current state of the simulation.
    ctx.clearRect(0, 0, canvas_s.x, canvas_s.y);
    ctx.save(); // save the matrix state.

    for (var i in scn.tiles) {
      var tile = scn.tiles[i];
      draw_tile(tile);
    }
    ctx.restore();

    // setup for hud drawing.
    ctx.font = '12pt Menlo';
    ctx.textAlign = 'center';
    ctx.textBaseline = 'middle';
    draw_info(sim_time, sim_interval, real_interval);
  }

  return {
    'render_scene': render_scene,
  };
}
 
