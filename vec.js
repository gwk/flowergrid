
'use strict'; // tell the browser to catch as many errors as possible.


// vector functions.

function Vec(x, y) {
  return { x: x, y: y };
}


function Vec_with_MA(m, a) {
  // create a vector with the given magnitude and direction.
  return Vec(Math.cos(a) * m, Math.sin(a) * m);
}


function mag(v) {
  return sqrt(sqr(v.x) + sqr(v.y));
}

function angle(v) {
  return Math.atan2(v.y, v.x);
}

function add(va, vb) {
  return Vec(va.x + vb.x, va.y + vb.y);
}


function sub(va, vb) {
  return Vec(va.x - vb.x, va.y - vb.y);
}


function mul(v, s) {
  return Vec(v.x * s, v.y * s);
}


function div(v, s) {
  return Vec(v.x / s, v.y / s);
}


function dist(va, vb) {
  return mag(sub(va, vb));
}


function norm(va, vb) {
  var delta = sub(va, vb);
  var d = mag(delta);
  if (d < eps) return Vec(1, 0); // pick an arbitrary direction; TODO: OR should we return Vec(0, 0)?
  return div(delta, d);
}


function vlerp(va, vb, t) {
  return Vec(lerp(va.x, vb.x, t), lerp(va.y, vb.y, t));
}

