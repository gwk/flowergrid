
'use strict'; // tell the browser to catch as many errors as possible.

String.prototype.fmt = function() {
  // add a method to the String class to format a string using a curly brace pattern.
  // "{0}" will get replaced by argument zero.
  var s = this // start with the format string.
  for (var i in arguments) {
    var re = new RegExp("\\{" + i + "\\}", "gm"); // global, multiline.
    s = s.replace(re, arguments[i]);
  }
  return s;
}


function log(item) {
  // utility function for printing out the value of an item; shorthand for console.log.
  console.log(item);
}


function clamp(val, min, max) {
  // clamp a value to be restricted inside of the range [min, max].
  return Math.max(min, Math.min(max, val));
}


function sqr(x) {
  return x * x;
}


function sqrt(x) {
  return Math.sqrt(x);
}


function mod(x, y) {
  // modulo function that handles negative x. y must be positive.
  if (x < 0) {
    return (x % y) + y;
  } else {
    return x % y;
  }
}


function norm_rad(r) {
  // normalize radians to the range of [0, pi2].
  return mod(r, pi2);
}


function reflect_x(r) {
  // reflect r about the x axis.
  return norm_rad(-r);
}


function reflect_y(r) {
  // reflect r about the y axis.
  return norm_rad(pi - r);
}


function reflect(r, around_r) {
  return nomr_rad(around_r - r) - around_r;
}


function rad_from_vec(v) {
  return Math.atan2(v.y, v.x); // atan2 takes y, x.
}


function lerp(a, b, t) {
  return (1 - t) * a + b * t;
}


function rand(l, h) {
  return l + Math.random() * (h - l);
}


function rand_unit() {
  return Math.random();
}


function log2(x) {
  return Math.log(x) / Math.log(2);
}


// color functions

function zero_pad(str, size) {
  while (str.length < size) str = "0" + str;
  return str;
}

function channel_to_hex(c) {
  // c is a color channel float in [0, 1].
  return zero_pad(Math.round(clamp(c, 0, 1) * 255).toString(16), 2);
}


function comps_to_col(r, g, b) {
  return "#" + channel_to_hex(r) + channel_to_hex(g) + channel_to_hex(b);
}


function rgb_to_col(rgb) {
  return comps_to_col(rgb.r, rgb.g, rgb.b);
}


function rand_col() {
  return comps_to_col(rand_unit(), rand_unit(), rand_unit());
}

