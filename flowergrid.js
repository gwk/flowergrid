
function load() {
  // main program setup.
  log('loading...');

  var renderer = load_renderer();

  // the 'scene' - the entire simulation state.
  var scn = {
    tiles: [],
  };

  function make_tile(x, y) {
    var i = scn.tiles.length;
    var obj = {
      i: i, // index.
      p: Vec(x, y), // position.
    };
    scn.tiles.push(obj);
    return obj;
  }

  function create_tiles() {
    for (var i = 0; i < tiles_s.x; i++) {
      for (var j = 0; j < tiles_s.y; j++) {
        make_tile(i, j);
      }
    }
  }

  function create_scene() {
    create_tiles();
  }

  function update_tile(tile, dt) {
    // update a tile given time delta dt.
  }

  function update(sim_time, sim_interval) {
    // update the state of the simulation.
    for (var i in scn.tiles) {
      var tile = scn.tiles[i];
      update_tile(tile, sim_interval);
    }
  }

  var sim_time = 0; // absolute simulation time.
  var sim_interval = .05; // sim time per step of simulation; user can change this dynamically.
  var real_interval = .05; // real time per step; user can change this dynamically.

  var camera_pos = Vec(0, 0);

  function render() {
    // convenient wrapper to render the scene.
    renderer.render_scene(sim_time, sim_interval, real_interval, scn);
  }

  function main_timer_callback() {
    // the main simulation callback; triggered by the window.setInterval timer.
    try {
      // the simulation never skips steps.
      // instead if the browser drops a callback, the sim just waits until the next step.
      // if we wanted a realtime sim, we would measure elapsed time between frames.
      sim_time += sim_interval; // update the simulation time first.
      update(sim_time, sim_interval);
      render();
    }
    catch(exc) {
      log(exc);
      stop();
      throw exc;
    }
  }

  function setup_animation() {
    // install main_timer_callback.
    if (interval_callback_id != -1) { // avoid installing multiple timers.
      log('ERROR: already have interval_callback_id')
      return;
    }
    interval_callback_id = window.setInterval(main_timer_callback, real_interval * 1000); // expects ms.
  }

  function teardown_animation() {
    // remove main_timer_callback.
    if (interval_callback_id != -1) { // there may not be any existing timer to remove.
      window.clearInterval(interval_callback_id);
      interval_callback_id = -1;
    }
  }
 
  function update_animation() {
    // if we are currently animating, remove the existing timer and reinstall with new interval.
    if (interval_callback_id != -1) {
      teardown_animation();
      setup_animation();
    }
  }

  function toggle_animation() {
    if (interval_callback_id == -1) { // start rendering.
      log('starting.');
      setup_animation();
    } else { // stop rendering.
      log('stopping.');
      teardown_animation();
    }
  }

  function keyPress(event) {
    // handle key events.
    var c = String.fromCharCode(event.charCode);
    var k = event.keyCode;
    if (k == 8800) { // 'opt =' increase sim_interval.
      sim_interval *= 1.1;
    } else if (k == 8211) { // 'opt -' decrease sim_interval.
      sim_interval /= 1.1;
    } else if (c == ' ') { // toggle animation.
      toggle_animation();
    } else if (c == 'd') { // toggle debug mode.
      render_mode_debug = !render_mode_debug;
    } else if (c == '=') { // zoom in.
      camera_scale *= 1.1;
    } else if (c == '-') { // zoom out.
      camera_scale /= 1.1;
    } else if (c == '+') { // speed up animation.
      real_interval /= 1.1;
      update_animation();
    } else if (c == '_') { // slow down animation.
      real_interval *= 1.1;
      update_animation();
    }
    if (interval_callback_id == -1) { // not animating.
      render();
    }
  }

  function keyDown(event) {
    // handle arrow key events and any others that do not get passed to keyPress().
    var code = event.keyCode;
    var cpx = camera_pos.x;
    var cpy = camera_pos.y;
    if (code == 37) { // left.
      camera_pos = Vec(cpx - 1 / (camera_scale * 32), cpy);
    } else if (code == 38) { // top.
      camera_pos = Vec(cpx, cpy - 1 / (camera_scale * 32));
    } else if (code == 39) { // right.
      camera_pos = Vec(cpx + 1 / (camera_scale * 32), cpy);
    } else if (code == 40) { // bottom.
      camera_pos = Vec(cpx, cpy + 1 / (camera_scale * 32));
    }
    //log('camera: t:({0}, {1}); s:{2}'.fmt(camera_pos.x, camera_pos.y, camera_scale));
    if (interval_callback_id == -1) { // not animating.
      render();
    }
  }

  // install both key event listeners.
  window.addEventListener('keypress', keyPress, false);
  window.addEventListener('keydown', keyDown, false);

  create_scene();
  render(); // render the initial state.
  //toggle_animation();
}
